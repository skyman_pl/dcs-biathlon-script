DCS v2.7.8.16140 https://www.digitalcombatsimulator.com/en/
MOOSE v2.7.7.1 https://github.com/FlightControl-Master/MOOSE

Functionality:
* uses MOOSE
* needs just one trigger to load the script
* gates/checkpoints represented by trigger zone in mission editor
* gates/checkpoints loaded automatically by Moose and listed explicitely by name
* one aicraft allowed
* run time measurement
* penalties for missing gates are calculated
* added a disqualification for missing too many gates over a set limit
* script detects if player or AI left the race area and resets everything
* only the racing player or AI is allowed in the race area during the race
