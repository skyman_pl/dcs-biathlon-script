--requires MOOSE initialized!

GateClass = 
{ 
zone = nil, 
checked = false, 
penalty = 0
}

function GateClass:New(object, zoneName, penalty)
	object = object or {}
	setmetatable(object, self)
	self.__index = self
	
	object.zone = ZONE:FindByName(zoneName)
	object.checked = false
	object.penalty = penalty
	
	return object
end

function GateClass:Reset()
	self.checked = false
end

BiathlonClass = 
{
gateList = {},
gateCount = 0,
timeStart = 0,
timeStop = 0,
missedGatesAllowed = 0,
raceInProgress,
unitOnTrack = nil,
unitCallsign = "",
trackAreaZoneList = {}
}

function BiathlonClass:New(object, gateList, missedGatesAllowed, trackAreaZonesNames)
	object = object or {}
	setmetatable(object, self)
	self.__index = self
	
	self.gateCount = #gateList
	self.gateList = gateList
	
	self.missedGatesAllowed = missedGatesAllowed
	
	for i,zone in ipairs(trackAreaZonesNames) do
		self.trackAreaZoneList[i] = ZONE:FindByName(zone)
	end
	
	for i,zone in ipairs(self.trackAreaZoneList) do
		MESSAGE:New( "Loaded zone: "..zone:GetName(),  20, "Biathlon" ):ToAll()
	end
	
	for i,gate in ipairs(self.gateList) do
		MESSAGE:New( "Loaded zone: "..gate.zone:GetName(),  20, "Biathlon" ):ToAll()
	end
	
	MESSAGE:New( self.missedGatesAllowed.." gates allowed to skip",  20, "Biathlon" ):ToAll()
	
	local zoneScheduler = SCHEDULER:New(nil, 
		function()
			timeCurrent = timer.getAbsTime()
			
			if self:IsRaceInProgress() then
				-- final gate
				if self.unitOnTrack:IsInZone(self.gateList[self.gateCount].zone) then
					self.timeStop = timeCurrent
					MESSAGE:New( "Race ended!",  120, "Biathlon" ):ToAll()
					self:PrintScore()
					self:ResetTrack()
				else
					-- intermidiate gates
					for i=2,self.gateCount-1 do
						if not self.gateList[i].checked and self.unitOnTrack:IsInZone(self.gateList[i].zone) then
							self.gateList[i].checked = true
							MESSAGE:New( self.gateList[i].zone:GetName().." checked",  20, "Biathlon" ):ToAll()
							break
						end
					end
					
					local hasUnitOnTrackLeft = true
					for i,unit in ipairs(self:GetActiveFlyingUnits()) do
						for j,zone in ipairs(self.trackAreaZoneList) do
							if unit:IsInZone(zone) then
								if self.unitOnTrack ~= nil and unit:GetName() == self.unitOnTrack:GetName() then
									hasUnitOnTrackLeft = false
								else
									MESSAGE:New( self:GetPlayerOrAiName(unit).." was on track illigally.",  30, "Biathlon" ):ToAll()
									unit:Explode()
								end
								break
							end
						end
					end
					
					if hasUnitOnTrackLeft then
						--unit dead
						MESSAGE:New( "Player just exited the race.",  120, "Biathlon" ):ToAll()
						self:ResetTrack()
					end
				end
			else
				-- starting gate
				local unitsInStartZone = self:GetFlyingUnitsInZone(self.gateList[1].zone)
				
				if #unitsInStartZone > 0 then
					self.unitOnTrack = unitsInStartZone[1]
					self.unitCallsign = self:GetPlayerOrAiName(self.unitOnTrack)
					self:SetRaceInProgress(true)
					self.timeStart = timeCurrent
					MESSAGE:New( self.unitCallsign.." entered the track! Everyone else keep out.",  120, "Biathlon" ):ToAll()
				end
			end
			
		end, {}, 1, 1)
	
	return object
end

function BiathlonClass:IsRaceInProgress()
	return self.unitOnTrack ~= nil and self.raceInProgress
end

function BiathlonClass:SetRaceInProgress(isRaceInProgress)
	self.raceInProgress = isRaceInProgress
end

function BiathlonClass:ResetTrack()
	MESSAGE:New( "Track will be reset.",  20, "Biathlon" ):ToAll()
	self:SetRaceInProgress(false)
	self.unitOnTrack = nil
	for i=1,self.gateCount do
		self.gateList[i]:Reset()
	end
end

function BiathlonClass:GetActiveFlyingUnits()
	return SET_UNIT:New():FilterActive():FilterCategories({"plane", "helicopter"}):FilterStart():GetSetObjects()
end

function BiathlonClass:GetFlyingUnitsInZone(zone)
	local flyingUnits = self:GetActiveFlyingUnits()
	local unitsInZone = {}
	for i,unit in ipairs(flyingUnits) do
		if unit:IsInZone(zone) then
			unitsInZone[#unitsInZone+1] = unit
		end
	end
	
	return unitsInZone
end

function BiathlonClass:GetPlayerOrAiName(unit)
	if unit:IsClient() then
		return unit:GetPlayerName()
	end
	
	return unit:GetName()
end

function BiathlonClass:PrintScore()
	local runtime = self.timeStop-self.timeStart
	local totalPenalty = 0
	local runtimeString = UTILS.SecondsToClock(runtime, false)
	local missedGatesCount = 0;
	local report = REPORT:New(self.unitCallsign.."'s score:")
	
	report:Add("")
	report:Add("Missed gates:")
	
	for i=2,self.gateCount-1 do
		if not self.gateList[i].checked then
			missedGatesCount = missedGatesCount + 1
			totalPenalty = totalPenalty + self.gateList[i].penalty
			report:AddIndent(self.gateList[i].zone:GetName())
		end
	end
	local totalScoreString = UTILS.SecondsToClock(runtime + totalPenalty, false)
	
	report:Add("")
	if missedGatesCount > self.missedGatesAllowed then
		report:Add("DISQUALIFIED for missing "..missedGatesCount.." gates ("..self.missedGatesAllowed.." allowed)")
	else
		report:Add("Total time: "..totalScoreString.." ("..runtimeString.." +"..totalPenalty.."s)")
	end
	
	MESSAGE:New(report:Text(),  180, "Biathlon" ):ToAll()
end

gateList = 
{
	GateClass:New(nil, "bramka-start", 0),
	GateClass:New(nil, "bramka-1", 90),
	GateClass:New(nil, "bramka-2", 90),
	GateClass:New(nil, "bramka-3", 90),
	GateClass:New(nil, "bramka-4", 90),
	GateClass:New(nil, "bramka-koniec", 0)
}

spectatorZones =
{
	"track_area-1",
	"track_area-2",
	"track_area-3",
	"track_area-4"
}

b = BiathlonClass:New(nil, gateList, 1, spectatorZones)